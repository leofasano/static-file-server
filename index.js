"use strict";
exports.__esModule = true;
var bodyParser = require("body-parser");
var helmet = require("helmet");
var cors = require("cors");
var express = require("express");
var path = require("path");
var uuid_1 = require("uuid");
var requestCount = 0;
var originsWhitelist = ['http://localhost:4200', 'http://localhost:4201'];
var port = 3001;
var corsOptions = {
    origin: function (origin, callback) {
        callback(null, true);
    }
};
var publicCorsOptions = {
    origin: function (origin, callback) {
        callback(null, true);
    }
};
var app = express();
// Call midlewares
app.use(cors(publicCorsOptions));
app.use(helmet());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(function (req, res, next) {
    requestCount++;
    var date = new Date();
    var origin = req.headers.origin;
    var time = ("00" + date.getHours()).slice(-2) + ":" + ("00" + date.getMinutes()).slice(-2) + ":" + ("00" + date.getSeconds()).slice(-2);
    console.log("\x1b[0m[\x1b[33m" + time + "\x1b[0m]: Request on \x1b[36m" + req.originalUrl + "\x1b[0m from \x1b[36m" + origin);
    var allowedOrigins = ['http://localhost:4200', 'http://localhost:4201'];
    if (allowedOrigins.includes(origin)) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    //res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    return next();
});
app.get("/works", function (req, res) {
    var works = [];
    var projectType = [
        "Project développement web",
        "Project d'infrastructure Cloud",
        "Project développement mobile",
        "Project développement smart watch",
    ];
    for (var i = 0; i < 20; i++) {
        var typeRand = Math.round(Math.random() * (projectType.length - 1));
        works.push({
            projectUuid: uuid_1.v4(),
            projectName: "Projet " + i,
            projectType: projectType[typeRand],
            projectMainImage: "http://" + req.headers.host + "/assets/images/bulldog.jpg",
            projectSecondaryImages: [
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg"
            ],
            projectMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            projectLink: "https://cognitives-labs.com",
            projectSecondaryDescription: {
                title: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
                text: "magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit."
            },
            projectThirdDescription: {
                title: "exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
            }
        });
    }
    res.send({
        works: works
    });
});
app.get("/works/:uuid", function (req, res) {
    var works = [];
    var projectType = [
        "Project développement web",
        "Project d'infrastructure Cloud",
        "Project développement mobile",
        "Project développement smart watch",
    ];
    for (var i = 0; i < 1; i++) {
        var typeRand = Math.round(Math.random() * (projectType.length - 1));
        works.push({
            projectUuid: req.params.uuid,
            projectName: "Projet " + i,
            projectType: projectType[typeRand],
            projectMainImage: "http://" + req.headers.host + "/assets/images/bulldog.jpg",
            projectSecondaryImages: [
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg",
                "http://" + req.headers.host + "/assets/images/bulldog.jpg"
            ],
            projectMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            projectLink: "https://cognitives-labs.com",
            projectSecondDescription: {
                title: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
                text: "magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit."
            },
            projectThirdDescription: {
                title: "exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
            }
        });
    }
    res.send({
        work: works[0]
    });
});
app.get("/news", function (req, res) {
    var news = {
        news: []
    };
    console.log("http://" + req.headers.host + "/assets/images/bulldog.jpg");
    for (var i = 0; i < 20; i++) {
        news.news.push({
            newsUUid: uuid_1.v4(),
            newsMainTitle: "Title of the news number " + i,
            newsMainImage: "http://" + req.headers.host + "/assets/images/news.jpg",
            newsMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            firstArticle: {
                image: "http://" + req.headers.host + "/assets/images/news.jpg",
                title: "Title of the first article",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
            },
            secondArticle: {
                image: "http://" + req.headers.host + "/assets/images/news.jpg",
                title: "Title of the first article",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
            },
            textArticle: [
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            ],
            newsConclusion: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
            newsNotes: {
                title: "Title of the news note",
                texts: [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
                ]
            }
        });
    }
    res.send(news);
});
// Serve static files
// @ts-ignore
var publicPath = path.join(__dirname, './public');
app.use(express.static(publicPath));
var listener = app.listen(port, null, null, function () {
    console.log("Server started on " + listener.address().address + ":" + listener.address().port);
});
/****************************
 *           API            *
     ****************************/ 
