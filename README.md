# static-file-server

## Install dependencies

Go to the root of the project and type the following command: 

```bash
npm i
```
## Run project

type the following command in the root of the project: 

```bash
npm start
```