import * as bodyParser from "body-parser"
import * as helmet from "helmet"
import * as cors from "cors"
import * as express from "express"
import * as path from "path"
import { v4 as uuid } from 'uuid'

let requestCount: number = 0
let originsWhitelist = ['http://localhost:4200', 'http://localhost:4201']

const port = 3001

var corsOptions = {
  origin: function (origin, callback) { 
    callback(null, true)
  }
}

var publicCorsOptions = {
  origin: function (origin, callback) { 
    callback(null, true)
  }
}

const app = express()

// Call midlewares
app.use(cors(publicCorsOptions))
app.use(helmet())
app.use(express.json({limit: '50mb'}))
app.use(express.urlencoded({limit: '50mb'}))
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

app.use((req, res, next) => {
  requestCount++
  let date = new Date()
  const origin: string = req.headers.origin as string;
  let time = ("00"+date.getHours()).slice(-2)+":"+("00"+date.getMinutes()).slice(-2)+":"+("00"+date.getSeconds()).slice(-2)
  console.log("\x1b[0m[\x1b[33m"+time+"\x1b[0m]: Request on \x1b[36m"+req.originalUrl+"\x1b[0m from \x1b[36m"+origin);
  
  
  const allowedOrigins = ['http://localhost:4200', 'http://localhost:4201'];
  if (allowedOrigins.includes(origin)) {
      res.setHeader('Access-Control-Allow-Origin', origin);
  }
  //res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, PUT, POST, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  return next();
});

app.get("/works", (req , res) => {
  let works: any[] = []
  let projectType: any[] = [
    "Project développement web",
    "Project d'infrastructure Cloud",
    "Project développement mobile",
    "Project développement smart watch",
  ]

  for(let i = 0; i < 20; i++){
    let typeRand = Math.round(Math.random()*(projectType.length-1))        

    works.push({
      projectUuid: uuid(),
      projectName: "Projet "+i,
      projectType: projectType[
        typeRand
      ],
      projectMainImage: "http://"+req.headers.host+"/assets/images/bulldog.jpg",
      projectSecondaryImages: [
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg"
      ],
      projectMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      projectLink: "https://cognitives-labs.com",
      projectSecondaryDescription: {
        title: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
        text: "magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit."
      },
      projectThirdDescription: {
        title: "exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
      }
    })
  }

  res.send({
    works: works
  })
})

app.get("/works/:uuid", (req , res) => {
  let works: any[] = []
  let projectType: any[] = [
    "Project développement web",
    "Project d'infrastructure Cloud",
    "Project développement mobile",
    "Project développement smart watch",
  ]

  for(let i = 0; i < 1; i++){
    let typeRand = Math.round(Math.random()*(projectType.length-1))        

    works.push({
      projectUuid: req.params.uuid,
      projectName: "Projet "+i,
      projectType: projectType[
        typeRand
      ],
      projectMainImage: "http://"+req.headers.host+"/assets/images/bulldog.jpg",
      projectSecondaryImages: [
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg",
        "http://"+req.headers.host+"/assets/images/bulldog.jpg"
      ],
      projectMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      projectLink: "https://cognitives-labs.com",
      projectSecondDescription: {
        title: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
        text: "magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit."
      },
      projectThirdDescription: {
        title: "exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
      }
    })
  }

  res.send({
    work: works[0]
  })
})

app.get("/news", (req, res) => {
  let news: any = {
    news: []
  }

  console.log("http://"+req.headers.host+"/assets/images/bulldog.jpg");
  

  for(let i = 0; i < 20; i++){
    news.news.push({
      newsUUid: uuid(),
      date: new Date('1995-12-17T03:24:00').toString(),
      newsMainTitle: "Title of the news number "+i,
      newsMainImage: "http://"+req.headers.host+"/assets/images/news.jpg",
      newsMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      firstArticle: {
        image: "http://"+req.headers.host+"/assets/images/news.jpg",
        title: "Title of the first article",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
      },
      secondArticle: {
        image: "http://"+req.headers.host+"/assets/images/news.jpg",
        title: "Title of the first article",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
      },
      textArticle: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
      ],
      newsConclusion: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
      newsNotes: {
        title: "Title of the news note",
        texts: [
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
        ]
      }
    })
  }

  res.send(news)
})


app.get("/news/:uuid", (req , res) => {
  let news: any[] = []

  news.push({
    newsUuid: req.params.uuid,
    date: new Date('1995-12-17T03:24:00').toString(),
    newsMainTitle: "Title of the news number "+Math.round(Math.random()*15),
    newsMainImage: "http://"+req.headers.host+"/assets/images/news.jpg",
    newsMainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    firstArticle: {
      image: "http://"+req.headers.host+"/assets/images/news.jpg",
      title: "Title of the first article",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
    },
    secondArticle: {
      image: "http://"+req.headers.host+"/assets/images/news.jpg",
      title: "Title of the first article",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam"
    },
    textArticle: [
      [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      ],
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
    ],
    newsConclusion: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
    newsNotes: {
      title: "Title of the news note",
      texts: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud magnam aliquam quaerat voluptatem. Ut enim ad minima veniam",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
      ]
    }
  })

  res.send({
    news: news[0]
  })
})


app.get("/page/fullstack", (req , res) => {
  res.send({
    mainTitle: "Studio fullstack",
    mainDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    mainImage: "http://"+req.headers.host+"/assets/images/news.jpg",
    article: [
      {
        title: "Lorem ipsum dolor sit amet",
        description: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p><p>laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p>"
      },
      {
        title: "Lorem ipsum dolor sit amet",
        description: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p><p>laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p>",
        image: "http://"+req.headers.host+"/assets/images/news.jpg"
      }
    ],
    bottomContent: {
      title: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium",
      image: "http://"+req.headers.host+"/assets/images/news.jpg",
      article: {
        title: "Qui dolorem ipsum quia dolor sit amet",
        description: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p><p>laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p>",
      }
    }
  })
})


// Serve static files
// @ts-ignore
var publicPath = path.join(__dirname, './public');
app.use(express.static(publicPath))

let listener = app.listen(port, null, null, () => {
    console.log("Server started on "+listener.address().address+":"+listener.address().port)
})
/****************************
 *           API            *
     ****************************/